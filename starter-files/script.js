
const startBtn = document.getElementById("startButton");

let allSessions = JSON.parse(localStorage.getItem('session')) || [];

let interval;
let totalTime;
let passedTime = 0;

startBtn.addEventListener("click", function (event) {
    event.preventDefault();
    
  const studyDuration = parseInt(
    document.getElementById("studyDuration").value
  );
  const breakDuration = parseInt(
    document.getElementById("breakDuration").value
  );

  const Session = {
    date: formateDate(),
    time: getTime(),
    study: `${studyDuration} minutes`,
    break: `${breakDuration} minutes`,
  };
  allSessions.push(Session);


  totalTime = (studyDuration + breakDuration) * 60; // sum of two input values turned into seconds.

  passedTime = 0; //I initialise passedTime to 0 so user can start the same session from the beggining.

  clearInterval(interval); // Clear any existing interval so only current session can run.

  interval = setInterval(progressBar, 1000); // Setting the interval to change the progress bar every second.
    
  localStorage.setItem('session',JSON.stringify(allSessions));

});


function progressBar() {
  passedTime++; //Increments the variable by 1 each time the function is called and tracks how many seconds have passed after the buton is clicked.
  const progressBar = document.getElementById("progressBar");
  const progress = (passedTime / totalTime) * 100; //That line turn the progressed time to progressed percentage.
    
  progressBar.style.width = progress + "%"; //Changes the progress bar width in proportion to time passed.
  
  
  // Stopping the timer when the total time has passed.
  // Alerting the Session in complete and after we click OK we the displaySession is called.
  if (passedTime == totalTime) {
    clearInterval(interval);
    alert("Session Complete!");
    displaySessions();
  }
}

function formateDate() {
    //
  const date = new Date();
  const year = date.getFullYear();
  const month = date.getMonth() + 1; //1 is added to make january 1 because by default in getMonth() method january is 0.
  const day = date.getDate();

  return `${day}/${month}/${year}`;
}
console.log(formateDate())

function getTime() {
    //Creates variables to get the date,hours,minutes,seconds and 12 hours clock
    //at hours and amPm are used type let variables because we need to change them
    //to change the format time.
  const date = new Date();
  let hours = date.getHours();  
  const minutes = date.getMinutes();
  const seconds = date.getSeconds();
  let amPm = "AM";

  //Changes the format of the time 12 hours format.
  if (hours == 0) {
    //At 00 hours we need to show 12 am
    hours === 12;
  } else if (hours > 12) {
    hours = hours % 12;
    amPm = "PM";
  }

  return `${hours}:${minutes}:${seconds} ${amPm}`;
}
console.log(getTime());

function displaySessions() {
  const list = document.getElementById("session");
  list.innerHTML = "";  //To prevent adding the previous sessions twice.

// The loop takes All objects from the array and acess all 
// the elements of the current object to display them inside 
// the new div created.
  for (let i = 0; i < allSessions.length; i++) {
    const div = document.createElement("div");
    const sessionHistory = allSessions[i];
    div.textContent = `Date: ${sessionHistory.date}, Time: ${sessionHistory.time}, Study: ${sessionHistory.study}, Break: ${sessionHistory.break}`;
    list.appendChild(div);
  }
}
